# Advanced Git Workshop
Lab 11: Recovering deleted objects

---

# Tasks

 - Deleting commits with git reset
 
 - Recovering deleted commits

---

## Preparations

 - Browse to the visualizing-git page:

```
http://git-school.github.io/visualizing-git
```

- Clean the editor (if necessary):

```
$ clear
```

---

## Deleting commits with git reset

  - Create some history:
```
$ git commit
$ git commit
$ git commit
$ git commit
$ git commit
```

  - Delete the last 2 commits using git rebase:
```
$ git reset --hard HEAD~2
```

  - Continue the work with some new commits:
```
$ git commit
$ git commit
```

---

## Recovering deleted commits

 - Let's inspect the reflog:
```
$ git reflog
```
```
> 14c237e HEAD@{0} commit:
> bac973b HEAD@{1} commit: 
> a967478 HEAD@{2} reset: moving to HEAD~2
> a339e10 HEAD@{3} commit: 
> c7561a4 HEAD@{4} commit: 
> a967478 HEAD@{5} commit: 
> c316e32 HEAD@{6} commit: 
> c085593 HEAD@{7} commit:
```

 - From the reflog we can know which commits were deleted (commits before reset):
```
> a339e10 HEAD@{3} commit: 
> c7561a4 HEAD@{4} commit:
```

 - To recover the deleted commits we will create a new branch:
```
$ git checkout HEAD@{3}
$ git branch recovered-commits
```

 - Then you will be able to merge, rebase or cherry-pick your changes